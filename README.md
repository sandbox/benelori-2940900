# Introduction
 Allows the user to create google charts as blocks with custom data.

 # Use case
 This module offers you the opportunity to create google charts with your data.
  It is very useful when you have to see some statistics or to be more visible some differences.
  For example you can use this to display the number of subscribed and unsubscribed users.

  # Instal

 In order to install this module, you can either install as you would normally install a contributed Drupal module.
Visit: https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules for further information.

 Or you can use composer:
    composer require drupal/google_charts

 # Usage example
After installing this project, create a class in src/Plugin/Block which will extends GoogleChartBlockBase. This class will have a specific annotation.
```PHP
namespace Drupal\your_module\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\google_charts\Block\GoogleChartBlockBase;

/**
 * Provides a 'CountryPieChartBlock' block.
 *
 * @Block(
 *  id = "country_pie_chart_block",
 *  admin_label = @Translation("Country pie chart block"),
 * )
 */
class CountryPieChartBlock extends GoogleChartBlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = parent::build();

    // Here you can add other variables, for example cache tags.

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
     // Here you have to return your own custom data.
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    // Return the pie chart title.
  }

}

```


The required methods which need to be inherited are:
- the getTitle(): this method is used to set the title for the pie chart;
- the getData(): this method is used to send your data.

The data returned by getData() methods need to be an array with key and value elements, for example:
  ['Drupal' => 100, 'Laravel' => 50; 'Symfony' => 25];

In the page template where you want to display you can add: {{ display_block('id') }}, for example: {{ display_block('country_pie_chart_block') }}
